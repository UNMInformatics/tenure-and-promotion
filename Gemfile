source 'https://rubygems.org'

gem 'rails'

# Bundle edge Rails instead:
#gem 'rails', :git => 'git://github.com/rails/rails.git'

gem 'sqlite3'
gem 'pg'

gem 'sass-rails'
gem 'less-rails'
gem 'coffee-rails'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer'

gem 'uglifier'

# File uploads
gem 'jquery-rails'
gem 'jquery-fileupload-rails'
gem 'paperclip'
gem 'fog'
gem 'httparty'

# Bootstrap Views
gem 'twitter-bootstrap-rails'
gem 'bootstrap_form'
gem 'twitter-bootstrap-rails-confirm'
gem 'select2-rails'
gem 'bootstrap-material-design'
gem 'font-awesome-rails'

# D3 Visualizations
gem 'd3_rails'

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Deploy with Capistrano
# gem 'capistrano'

# To use debugger
# gem 'debugger'

#CloudConvert
gem 'cloudconvert'

#CAS Authentication
gem "rubycas-client-rails", git: "git://github.com/rubycas/rubycas-client-rails.git"

#Puma Server for Concurrency
gem 'puma'

#Prawn for PDF generation, 0.13 for merging
gem 'prawn'

# My PDF library
gem 'jpdfunite'
#gem 'prawn-templates'#, :require => "prawn-templates"#, '0.13'

#Mandril for sending mail
gem 'mandrill-api'

# Breadcrumbs for navigation
gem 'crummy'

# FullCalendar for date interaction
gem 'fullcalendar_engine'

# Debug tools
gem 'pry-rails'

# Because Rails insists on generating slim views
gem 'slim'

# For file upload icons
gem 'rails-file-icons'

# For larger cookies
gem 'activerecord-session_store'

# To enforce correct order and allow reordering
gem 'ranked-model'

# To get ERD
gem 'rails-erd'

# For Redmine Integration
gem 'activeresource'

group :test, :development do
  # For RSpec Testing
  gem 'rspec-rails'
  gem 'factory_girl_rails', require: false
  gem 'faker'
end
