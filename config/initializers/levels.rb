LEVELS = [:candidate,
          :department_committee,
          :department_admin,
          :college_committee,
          :college_admin,
          :provost_committee,
          :provost_admin,
          :archive]
