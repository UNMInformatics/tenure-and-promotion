class RemoveLevelFromRole < ActiveRecord::Migration
  def change
    remove_column :roles, :level, :string
  end
end
