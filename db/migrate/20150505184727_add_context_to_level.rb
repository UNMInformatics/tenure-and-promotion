class AddContextToLevel < ActiveRecord::Migration
  def change
    add_column :levels, :context, :string
  end
end
