class AddShareToUser < ActiveRecord::Migration
  def change
    add_column :users, :share, :boolean
  end
end
