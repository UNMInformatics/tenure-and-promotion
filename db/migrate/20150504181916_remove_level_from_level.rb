class RemoveLevelFromLevel < ActiveRecord::Migration
  def change
    remove_column :levels, :level, :string
  end
end
