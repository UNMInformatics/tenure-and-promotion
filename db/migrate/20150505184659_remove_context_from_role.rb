class RemoveContextFromRole < ActiveRecord::Migration
  def change
    remove_column :roles, :context, :string
  end
end
