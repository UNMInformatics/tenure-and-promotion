class RemoveLevelFromCategory < ActiveRecord::Migration
  def change
    remove_column :categories, :level, :string
  end
end
