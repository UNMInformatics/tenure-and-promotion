class AddLevelToSection < ActiveRecord::Migration
  def change
    add_reference :sections, :level, index: true, foreign_key: true
  end
end
