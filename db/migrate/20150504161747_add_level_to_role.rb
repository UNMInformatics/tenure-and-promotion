class AddLevelToRole < ActiveRecord::Migration
  def change
    add_reference :roles, :level, index: true, foreign_key: true
  end
end
