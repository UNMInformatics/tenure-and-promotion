class AddContextToRole < ActiveRecord::Migration
  def change
    add_column :roles, :context, :string
  end
end
