class RemoveLevelFromSection < ActiveRecord::Migration
  def change
    remove_column :sections, :level, :string
  end
end
