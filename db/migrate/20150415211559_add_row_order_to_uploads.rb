class AddRowOrderToUploads < ActiveRecord::Migration
  def change
    add_column :uploads, :row_order, :integer
  end
end
