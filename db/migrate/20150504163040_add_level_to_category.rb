class AddLevelToCategory < ActiveRecord::Migration
  def change
    add_reference :categories, :level, index: true, foreign_key: true
  end
end
