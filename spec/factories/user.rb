FactoryGirl.define do

  Role.all.each do |role|

    factory role.name.parameterize.underscore.to_sym, class: User do
      name  { Faker::Name.name }
      netid { Faker::Name.name.parameterize.underscore }
      roles { [role] }
    end

  end

end
