require 'rails_helper'

include FactoryGirl::Syntax::Methods

RSpec.describe User, type: :model do
  it "Creates Users" do
		users = Role.all.map { |role| create(role.name.parameterize.underscore.to_sym) }
		expect(users.map{ |user| User.find(user.id) }.all?).to eq(true)
  end
end
