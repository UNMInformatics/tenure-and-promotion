require 'active_resource'

class RedmineCollection < ActiveResource::Collection
  def initialize(parsed = {})
    @elements = parsed.values.first
  end
end

class Issue < ActiveResource::Base
  self.site = ENV["REDMINE_SERVER"]
  self.user = ENV["REDMINE_USER"]
  self.password = ENV["REDMINE_PASSWORD"]
  self.collection_parser = RedmineCollection
  self.include_root_in_json = true
end

