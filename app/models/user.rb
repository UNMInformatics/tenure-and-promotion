class User < ActiveRecord::Base

  attr_accessor :added_role

  belongs_to :department
  belongs_to :college

  enum phase: LEVELS

  # A user may have multiple roles
  has_many :user_roles, dependent: :destroy
  has_many :roles, through: :user_roles

  #Categories belong to each user.
  has_many :categories, dependent: :destroy
  after_create :create_categories

  # Recusals are a self-join between users
  has_many :recusals, dependent: :destroy

  has_attached_file :dossier
  do_not_validate_attachment_file_type :dossier

  after_rollback :multiple_role
  # A net ID may only be registered once
  validates :netid, presence: true, uniqueness: true

  # For Homepage
  def next_phase
    Level.find_by_level(self[:phase] + 1)
  end

  def phase_complete!
    update phase: self[:phase] + 1
  end

  def check_context(role, candidate)
    context = role.level.context
    context != "Individual" &&
    context == "University" ||
    public_send(context.downcase) == candidate.public_send(Level.find_by_level(candidate[:phase]).context.downcase)
  end

  private

  def multiple_role
    existing = User.find_by_netid(netid)
    add_role = UserRole.new(user: existing, role: roles.first) if existing
    existing && existing.department || existing.update(department_id: department_id)
    add_role && add_role.save && @added_role = true
  end

  def create_categories
    # Don't do this for people at higher levels
    return unless department
    # Read the categories from the database
    sections = department.department_sections.map(&:section)
    sections.each { |section| categories.create(name: section.name, level: section.level, row_order: section.row_order) }
  end

end
