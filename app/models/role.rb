class Role < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  # Each role is associated with a level
  belongs_to :level

  # Each role has many users
  has_many :user_roles, dependent: :destroy
  has_many :users, through: :user_roles

  def links
    candidate = ["Faculty Candidates", users_path(role: link_role("Candidate")), "Review"]

     # Role                    Link text                          Link Path
    { tech:                 [["Roles",                          roles_path],
                             ["Colleges",                       colleges_path],
                             ["Departments",                    departments_path],
                             ["Calendar",                       cal_path],
                             ["Recusals",                       recusals_path]],

      provost:              [["Provost",                        users_path(role: link_role("Provost"))],
                             ["Provost's Faculty Committee",    users_path(role: link_role("Provost Committee"))],
                             ["College Administrators",         colleges_path(role: link_role("College Admin"))],
                             ["Faculty Candidates", colleges_path(role: link_role("Candidate")), "Review"]],

      provost_committee:    [["Faculty Candidates", colleges_path(role: link_role("Candidate")), "Review"]],

      college_admin:        [["College-Level T&P Committee",    users_path(role: link_role("College Committee"))],
                             ["Department Administrators",      users_path(role: link_role("Department Admin"))],
                             ["College Dean",                   users_path(role: link_role("College Admin"))],
                             candidate],

      college_committee:    [candidate],

      department_admin:     [["Department-Level T&P Committee", users_path(role: link_role("Department Committee"))],
                             ["Department Chair",               users_path(role: link_role("Department Admin"))],
                             candidate],

      department_committee: [candidate],

      candidate:            [["Your Dossier",                   categories_path]] }[level.name.parameterize.underscore.to_sym]
  end

  def link_role(name)
    Level.find_by_name(name).id
  end

  def method_missing(*args)
    super unless args.first =~ /\?$/
    level == Level.find_by_name(args.first.to_s.delete("?").titleize)
  end

  # This checks that the current role can read a dossier
  def same_level? candidate
    # Reviewer context          Candidate context
    level.context == Level.find_by_level(candidate[:phase]).context && level.level >= candidate[:phase]
  end

end
