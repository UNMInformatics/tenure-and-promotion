class Upload < ActiveRecord::Base

  include Rails.application.routes.url_helpers
  include RankedModel

  ranks :row_order

  belongs_to :category
  has_one :convert, dependent: :destroy
  has_attached_file :upload
  #Don't check the attachment type, it could be anything!
  do_not_validate_attachment_file_type :upload


  #Adds simple attributes to the uploaded file
  def to_jq_upload
    {
      "id"            => id,
      "name"          => read_attribute(:upload_file_name),
      "size"          => read_attribute(:upload_file_size),
      "url"           => upload.url(:original),
      "delete_url"    => category_upload_path(category_id,self),
      "delete_type"   => "DELETE",
      "thumbnail_url" => ActionController::Base.helpers.image_url(Icon.for_filename upload_file_name) 
    }
  end
end
