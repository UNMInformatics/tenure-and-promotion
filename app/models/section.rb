class Section < ActiveRecord::Base

  include RankedModel

  belongs_to :level

  has_many :department_sections, dependent: :destroy

  ranks :row_order

  def pdf?
    pdf ? "Yes" : "No"
  end

end
