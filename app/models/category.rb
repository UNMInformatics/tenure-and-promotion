class Category < ActiveRecord::Base

  include RankedModel

  # Categories belong to a level
  belongs_to :level

  # Categories may need to be re-ordered
  ranks :row_order

  # Each user has Categories
  belongs_to :user
  # Each category has files (Uploads) attached to it
  has_many :uploads, dependent: :destroy
  has_many :converts, dependent: :destroy

  def pdf?
    Section.find_by_name(name).pdf
  end

end
