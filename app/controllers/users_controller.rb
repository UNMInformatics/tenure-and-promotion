class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :set_options, only: [:new, :edit, :index, :create]
  before_action :only_admin!, except: [:index, :contextualize]

  add_crumb "Faculty", '/users' 

  # GET /users
  def index
    @role = Level.find(session[:role_context])
    set_role_names
    set_available_users
  end

  # GET /users/1
  def show
    add_crumb @user.name, user_path
  end

  # GET /users/new
  def new
    @user = User.new
    set_college
  end

  # GET /users/1/edit
  def edit
    add_crumb @user.name, user_path
  end

  # POST /users
  def create
    @user = User.new user_params
    @user.college = current_user.college unless level >= 6
    @user.department = current_user.department unless level >= 4

    if @user.save
      Welcome.candidate_mail(recipient: @user).deliver
      redirect_to users_path, notice: "#{Level.find(session[:role_context]).name.humanize} was successfully created."
    else
      if @user.added_role
        redirect_to users_path, notice: "Multiple Role added"
      else
        set_college
        #Go back to the page if error
        render :new
      end
    end
  end

  # PATCH/PUT /users/1
  def update
    add_crumb @user.name, user_path
    if @user.update(user_params)
      redirect_to users_path, notice: 'Information updated successfully'
    else
      render :edit
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
    redirect_to users_url, notice: 'User was successfully destroyed.'
  end

  def contextualize
    candidate = User.find params[:user_id]
    if current_user.check_context(role, candidate)
      session[:context_user] = candidate
      redirect_to categories_path, notice: "Working in context of #{candidate.name}"
    else
      redirect_to users_path
      flash[:error] = 'This user is not available'
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find params[:id]
  end

  def set_options
    # This just ensures the URL param cannot be spoofed.
    valid_roles = Role.all.select { |valid_role| (level - valid_role.level.level).between? 0,2 }
    # Don't show admins at same level, and don't show non-admins at lower level
    valid_roles.select! { |valid_role| valid_role.candidate? || (valid_role.level.context == role.level.context) ^ valid_role.admin? }
    # This sets the context based on prior URL parameters
    session[:role_context] = params[:role]    ? params[:role].to_i    : session[:role_context]
    session[:college]      = params[:college] ? params[:college].to_i : session[:college]
    # This checks the session variables
    @roles = valid_roles.select { |role| role.level.id == session[:role_context] }
    set_college
    @departments = Department.all
  end

  # Only allow a trusted parameter "white list" through.
  def user_params
    @role = Role.find(params.require(:user)[:roles][:role_id])
    params.require(:user).permit(:netid, :name, :department_id, :college_id).merge(roles: [@role])
  end

  # This method sets the @users instance variable to reflect the users that
  # should be shown. It bases the @college variable on a session variable which
  # is set by a URL parameter from the home page.
  def set_available_users
    # Only show users at or below the logged in user's level
    # Note this is a collection of UserRole JOIN objects, not Users!    
    users = UserRole.all.select { |u| u.role.level.level <= level }
    set_college
    if role.department_admin?
      # List Users within department
      @department = current_user.department
      users.select! { |u| u.user.department == @department }
    elsif role.college_admin?
      # List Users within college
      users.select! { |u| @college.include? u.user.college }
      users.select! { |user| user.role.candidate? || (user.role.level.context == role.level.context) ^ user.role.admin? }
    end
 
    # List Users within selected college
    @college && users.select! { |u| @college.include? u.user.college }
    users.select! { |u| u.role.level.id == @role.id && u.role != role }
    @users = users
  end

  # This method sets the college instance variable. If a college session
  # variable is present, it uses that. If not, and at the Provost level or
  # higher, it sets the value to all colleges. Otherwise, it sets it to the
  # college the current user belongs to.
  def set_college
    @college = session[:college] ? [College.find(session[:college])] : @college
    @college ||= level >= 6 ? College.all : [current_user.college]
  end

  # This method can be used to authorize admin users
  def only_admin!
    redirect_to contact_path unless role.admin?
  end

  def set_role_names
    @names = @roles.map(&:name).join(", ") if @roles.any?
    @names ||= @role.name.pluralize
  end

end
