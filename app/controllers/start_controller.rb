class StartController < ApplicationController

  add_crumb "License", "/"

  def index
    redirect_to set_role_path unless session[:role]
  end

  def logout
    reset_session
    RubyCAS::Filter.logout self
  end

  def set_role
    return logout unless current_user && current_user.roles.any?
    params[:role] && session[:role] = current_user.roles.find(params[:role])
    session[:role] = current_user.roles.first if current_user.roles.length == 1
    render layout: "newuser" unless session[:role]
    redirect_to root_path if session[:role]
  end

end
