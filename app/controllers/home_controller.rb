class HomeController < ApplicationController

  def index
    redirect_to categories_path if role.candidate?
    @links = role.links 
    session[:college] = nil
  end

end
