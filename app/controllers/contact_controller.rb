class ContactController < ApplicationController
  def index
    add_crumb "Contact Us", contact_path
    find_admin
  end

  def find_admin
    admins = User.select { |user| user.roles.map{ |admin_role| admin_role.level.level }.max > level && user.roles.any?(&:admin?) }
    # Sort these to make the subsequent search easier to understand.
    admins.sort_by! { |admin| admin.roles.select(&:admin?).map{ |admin_role| admin_role.level.level }.max }
    # Search for a department admin, if none, search for college admin, if none,
    # search for provost admin or tech.
    @admin = admins.find do |admin|
      admin.department == current_user.department ||
        admin.college == current_user.college ||
        admin.roles.map{ |role| role.level.level }.max > 5
    end
    # If nothing else is found, use the Provost Admin.
    @admin ||= User.find { |user| user.roles.any? { |role| role.provost? && role.admin? } }
    # Find the lowest applicable role. If that fails, find the highest role that
    # the admin has.
    @role = @admin.roles.find { |admin_role| admin_role.level.level > level && admin_role.admin? } || @admin.roles.max_by(&:level)
  end
end
