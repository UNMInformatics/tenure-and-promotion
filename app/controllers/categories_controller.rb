class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  before_action :set_user
  before_filter :check_phase!

  add_crumb 'Dossier', '/sections'
  # GET /categories
  def index
    #Categories each belong to a user
    @categories = @user.categories.rank(:row_order).select { |category| role.level == category.level } 
    @total = @categories.map{ |c| c.uploads.length}.reduce(:+)
    @button = button
    redirect_to compile_path if @categories.empty?
  end

  # GET /categories/1
  def show
    add_crumb @category.name, category_path
  end

  # GET /categories/new
  def new
    add_crumb 'New Section', new_category_path
    @category = @user.categories.new
  end

  # GET /categories/1/edit
  def edit
    add_crumb @category.name, category_path
  end

  # POST /categories
  def create
    @category = @user.categories.new category_params

    if @category.save
      redirect_to @category, notice: 'Category was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /categories/1
  def update
    add_crumb @category.name, category_path
    if @category.update category_params
      redirect_to @category, notice: 'Category was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /categories/1
  def destroy
    @category.destroy
    redirect_to categories_url, notice: 'Category was successfully destroyed.'
  end

  def phase_complete
    @user.phase_complete!
    if @user[:phase].odd?
      redirect_to contact_path, notice: "This dossier was submitted to the #{@user.phase.titleize}."
    else
      redirect_to categories_path, notice: "This dossier was closed to the committee."
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_category
      set_user
      @category = @user.categories.find params[:id]
    end

    # Only allow a trusted parameter "white list" through.
    def category_params
      params.require(:category).permit :name, :user_id
    end

    def set_user
      @user = context_user
    end

    def button
      [:text, :confirm].zip({ department_committee: ["Submit final Dossier to Department Committee", "I acknowledge that my dossier is complete and ready for review. Any additional changes must be made through an administrator"],
        department_admin: ["Close Dossier to Department Committee", "This will close the dossier permanently to the Department Committee staff, so only the admin and chair can view the Dossier."],
        college_committee: ["Submit final Dossier to College Committee", "I acknowledge that all Department documents are correctly entered and are ready for review by the College. Any further changes must be made through an administrator."],
        college_admin: ["Close Dossier to College Commitee", "This will close the dossier permanently to the College Commitee staff, so only the admin and dean can view the Dossier."],
        provost_committee: ["Submit final Dossier to Provost Committee", "I acknowledge that all College documents are correctly entered and are ready for review by the Provost Committee. Any further changes must be made through and administrator."],
        provost_admin: ["Close Dossier to Provost Committee", "This will close the dossier permanently to the Provost Committee, so only the Provost Admin, Senior Vice Provost, and Provost may view the Dossier."],
        archive: ["Close and Archive final Dossier", "I acknowledge that all documents for this dossier are complete and correct, and have been fully reviewed. After clicking yes, this dossier will be archived and cannot be viewed or edited anymore through this application."]
      }[User.phases.key(context_user[:phase] + 1).to_sym]).to_h
    end

end
