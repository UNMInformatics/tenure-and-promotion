class ApplicationController < ActionController::Base
  protect_from_forgery
  #Require CAS to log in.
  before_filter :cas_filter
  before_action :mycrumbs, :agreement
  #These methods can be used in views too!
  helper_method :current_user, :role, :level, :context_user

  #Default Crumbs
  def mycrumbs
    add_crumb '<i class="fa fa-home"></i>Home'.html_safe, root_path 
  end

  #Put RubyCAS filter in a method, in case it needs to be escaped
  def cas_filter
    RubyCAS::Filter.filter self
  end

  #Redirect to license page if you haven't already been there
  def agreement
    redirect_to license_path unless session[:agreement]
    session[:agreement] = true
  end

  #Take the Net ID and find the ActiveRecord object that is made for it
  def current_user
    User.find_by_netid(session[:cas_user])
  end

  def role
    session[:role] && Role.find(session[:role].id)
  end

  def level
    role && role.level.level
  end


  def context_user
    session[:context_user] ? User.find(session[:context_user]) : current_user
  end

  def check_phase!
    unless role.same_level? context_user
      redirect_to contact_path
      flash[:error] = "This dossier is currently at the #{context_user.phase.humanize} review phase."
    end
  end

end
